const markdownIt = require('markdown-it')({
  html: true,
  linkify: true,
});

const createSchemaFaq = (faqSchemaData: any) => {
  const entities = faqSchemaData.map(
    (schema: { question: any; answer: any; cta: { url: any; text: any } }) => {
      return {
        '@type': 'Question',
        name: `<p>${schema.question}</p>`,
        acceptedAnswer: {
          '@type': 'Answer',
          text: markdownIt.render(schema.answer),
        },
      };
    },
  );

  return {
    '@context': 'http://schema.org',
    '@type': 'FAQPage',
    mainEntity: entities,
  };
};

export default createSchemaFaq;
